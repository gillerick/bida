package com.bida.models;

import java.time.LocalDateTime;

public class Product {
    public Integer id;
    public String name;
    public String sub_category_id;
    public LocalDateTime createdAt;
    public LocalDateTime updatedAt;
}
