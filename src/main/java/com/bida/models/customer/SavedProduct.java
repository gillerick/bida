package com.bida.models.customer;

import com.bida.models.Product;

import java.time.LocalDateTime;

public class SavedProduct {
    public int id;
    public Customer customer;
    public Product product;
    public LocalDateTime createdAt;
}
