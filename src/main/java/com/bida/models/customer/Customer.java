package com.bida.models.customer;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

public class Customer implements Serializable {
    public Integer customerId;
    public String name;
    public String email;
    public String phone;
    public String gender;
    public Integer resetCode;
    public Integer verifyCode;
    public String token;
    public LocalDateTime createdAt;
    public LocalDateTime updatedAt;

    public Customer (Integer customerId, String name, String email, String phone, String gender, Integer resetCode, Integer verifyCode, String token, LocalDateTime createdAt, LocalDateTime updatedAt) {
        this.customerId = customerId;
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.gender = gender;
        this.resetCode = resetCode;
        this.verifyCode = verifyCode;
        this.token = token;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    @Override
    public boolean equals (Object o) {
        if (this == o) return true;
        if (!(o instanceof Customer)) return false;
        Customer customer = (Customer) o;
        return Objects.equals(getCustomerId(), customer.getCustomerId()) && Objects.equals(getName(), customer.getName()) && Objects.equals(getEmail(), customer.getEmail()) && Objects.equals(getPhone(), customer.getPhone()) && Objects.equals(getGender(), customer.getGender()) && Objects.equals(getResetCode(), customer.getResetCode()) && Objects.equals(getVerifyCode(), customer.getVerifyCode()) && Objects.equals(getToken(), customer.getToken()) && Objects.equals(getCreatedAt(), customer.getCreatedAt()) && Objects.equals(getUpdatedAt(), customer.getUpdatedAt());
    }

    @Override
    public int hashCode () {
        return Objects.hash(getCustomerId(), getName(), getEmail(), getPhone(), getGender(), getResetCode(), getVerifyCode(), getToken(), getCreatedAt(), getUpdatedAt());
    }

    public Integer getCustomerId () {
        return customerId;
    }

    public String getName () {
        return name;
    }

    public String getEmail () {
        return email;
    }

    public String getPhone () {
        return phone;
    }

    public String getGender () {
        return gender;
    }

    public Integer getResetCode () {
        return resetCode;
    }

    public Integer getVerifyCode () {
        return verifyCode;
    }

    public String getToken () {
        return token;
    }

    public LocalDateTime getCreatedAt () {
        return createdAt;
    }

    public LocalDateTime getUpdatedAt () {
        return updatedAt;
    }
}
