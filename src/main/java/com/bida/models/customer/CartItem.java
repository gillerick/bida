package com.bida.models.customer;

import java.time.LocalDateTime;

public class CartItem {
    public Integer id;
    public Integer productId;
    public Integer quantity;
    public LocalDateTime createdAt;
    public LocalDateTime updatedAt;
}
