package com.bida.models.customer;

import java.io.Serializable;
import java.time.LocalDateTime;

public class Payment implements Serializable {
    enum PaymentType {Card, Mobile, Money, PromoCode };

    public Integer id;
    public Enum<PaymentType> type;
    public String provide;
    public String AccountNumber;
    public String Expiry;
    public LocalDateTime createdAt;
    public LocalDateTime updatedAt;
}
