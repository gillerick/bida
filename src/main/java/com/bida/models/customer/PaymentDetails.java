package com.bida.models.customer;

import java.io.Serializable;
import java.time.LocalDateTime;

public class PaymentDetails implements Serializable {
    enum paymentStatus {Pending, Completed, Failed};
    public Integer id;
    public Float amount;
    public Enum<paymentStatus> status;
    public LocalDateTime createdAt;
    public LocalDateTime updatedAt;
}
