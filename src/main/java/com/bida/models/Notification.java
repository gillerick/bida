package com.bida.models;

import com.bida.models.customer.Customer;

import java.time.LocalDateTime;

public class Notification {
    public Integer id;
    public Customer customer;
    public String category;
    public String description;
    public LocalDateTime createdAt;
    public LocalDateTime deletedAt;
}
