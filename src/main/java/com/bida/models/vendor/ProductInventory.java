package com.bida.models.vendor;

import com.bida.models.Product;
import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import io.vertx.mutiny.pgclient.PgPool;
import io.vertx.mutiny.sqlclient.Row;
import io.vertx.mutiny.sqlclient.Tuple;

import javax.ws.rs.GET;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.stream.StreamSupport;

public class ProductInventory {
    public Long id;
    public Vendor vendor;
    public Product product;
    public Float price;
    public Long quantity;
    public Boolean visibility;
    public InventoryStatus status;
    public LocalDateTime createdAt;
    public LocalDateTime updatedAt;
    public LocalDateTime deletedAt;

    public Long getId () {
        return id;
    }

    public Vendor getVendor () {
        return vendor;
    }

    public Product getProduct () {
        return product;
    }

    public Float getPrice () {
        return price;
    }

    public Long getQuantity () {
        return quantity;
    }

    public Boolean getVisibility () {
        return visibility;
    }

    public InventoryStatus getStatus () {
        return status;
    }

    public LocalDateTime getCreatedAt () {
        return createdAt;
    }

    public LocalDateTime getUpdatedAt () {
        return updatedAt;
    }

    public LocalDateTime getDeletedAt () {
        return deletedAt;
    }

    public ProductInventory (Long id, Vendor vendor, Product product, Float price, Long quantity, Boolean visibility, InventoryStatus status, LocalDateTime createdAt, LocalDateTime updatedAt, LocalDateTime deletedAt) {
        this.id = id;
        this.vendor = vendor;
        this.product = product;
        this.price = price;
        this.quantity = quantity;
        this.visibility = visibility;
        this.status = status;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.deletedAt = deletedAt;
    }

    public void setId (Long id) {
        this.id = id;
    }

    public void setVendor (Vendor vendor) {
        this.vendor = vendor;
    }

    public void setProduct (Product product) {
        this.product = product;
    }

    public void setPrice (Float price) {
        this.price = price;
    }

    public void setQuantity (Long quantity) {
        this.quantity = quantity;
    }

    public void setVisibility (Boolean visibility) {
        this.visibility = visibility;
    }

    public void setStatus (InventoryStatus status) {
        this.status = status;
    }

    public void setCreatedAt (LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public void setUpdatedAt (LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public void setDeletedAt (LocalDateTime deletedAt) {
        this.deletedAt = deletedAt;
    }

    @Override
    public boolean equals (Object o) {
        if (this == o) return true;
        if (!(o instanceof ProductInventory)) return false;
        ProductInventory that = (ProductInventory) o;
        return getId().equals(that.getId()) && getVendor().equals(that.getVendor()) && getProduct().equals(that.getProduct()) && getPrice().equals(that.getPrice()) && Objects.equals(getQuantity(), that.getQuantity()) && Objects.equals(getVisibility(), that.getVisibility()) && getStatus() == that.getStatus() && Objects.equals(getCreatedAt(), that.getCreatedAt()) && Objects.equals(getUpdatedAt(), that.getUpdatedAt()) && Objects.equals(getDeletedAt(), that.getDeletedAt());
    }

    @Override
    public int hashCode () {
        return Objects.hash(getId(), getVendor(), getProduct(), getPrice(), getQuantity(), getVisibility(), getStatus(), getCreatedAt(), getUpdatedAt(), getDeletedAt());
    }

    public ProductInventory (Long id, Float price, Long quantity, Boolean visibility, String status, LocalDateTime created_at, LocalDateTime updated_at, LocalDateTime updatedAt, LocalDateTime deleted_at) {
        super();
    }

    @Override
    protected Object clone () throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public String toString () {
        return super.toString();
    }

    public static Multi<ProductInventory> findAll (PgPool client) {
        return client
                .query("SELECT id, status FROM product_inventory")
                .execute()
                .onItem()
                .transformToMulti(set -> Multi.createFrom().iterable(set))
                .onItem()
                .transform(ProductInventory::from);

    }

    public static Uni<ProductInventory> findByID(PgPool client, Long id){
        return client
                .preparedQuery("SELECT id, status FROM product_inventory WHERE id = $1")
                .execute(Tuple.of(id))
                .onItem()
                .transform(m -> m.iterator().hasNext() ? from(m.iterator().next()) : null);
    }



    public static ProductInventory from (Row row) {
        return new ProductInventory(row.getLong("id"), row.getFloat("price"), row.getLong("quantity"),
                row.getBoolean("visibility"), row.getString("status"), row.getLocalDateTime("created_at"),
                row.getLocalDateTime("updated_at"), row.getLocalDateTime("updated_at"), row.getLocalDateTime("deleted_at"));
    }


}
