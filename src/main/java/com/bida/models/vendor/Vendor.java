package com.bida.models.vendor;

public class Vendor {
    public Integer id;
    public String name;
    public String email;
    public String phone;
    public String business;
    public String workingHours;

}
