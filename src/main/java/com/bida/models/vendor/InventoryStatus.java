package com.bida.models.vendor;

public enum InventoryStatus {
    OUT_OF_STOCK, IN_STOCK, RUNNING_LOW;
}
