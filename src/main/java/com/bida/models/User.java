package com.bida.models;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.quarkus.jackson.ObjectMapperCustomizer;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

public class User implements ObjectMapperCustomizer {
    public Integer id;
    public String userName;
    public LocalDateTime createdAt;
    public LocalDateTime updatedAt;
    public LocalDateTime deletedAt;

    @Override
    public boolean equals (Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        User user = (User) o;
        return getId().equals(user.getId()) && Objects.equals(getUserName(), user.getUserName()) && Objects.equals(getCreatedAt(), user.getCreatedAt()) && getUpdatedAt().equals(user.getUpdatedAt()) && getDeletedAt().equals(user.getDeletedAt());
    }

    @Override
    public int hashCode () {
        return Objects.hash(getId(), getUserName(), getCreatedAt(), getUpdatedAt(), getDeletedAt());
    }

    public Integer getId () {
        return id;
    }

    public void setId (Integer id) {
        this.id = id;
    }

    public String getUserName () {
        return userName;
    }

    public void setUserName (String userName) {
        this.userName = userName;
    }

    public LocalDateTime getCreatedAt () {
        return createdAt;
    }

    public void setCreatedAt (LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getUpdatedAt () {
        return updatedAt;
    }

    public void setUpdatedAt (LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public LocalDateTime getDeletedAt () {
        return deletedAt;
    }

    public void setDeletedAt (LocalDateTime deletedAt) {
        this.deletedAt = deletedAt;
    }

    public User (Integer id, String userName, LocalDateTime createdAt, LocalDateTime updatedAt, LocalDateTime deletedAt) {
        this.id = id;
        this.userName = userName;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.deletedAt = deletedAt;
    }

    @Override
    public void customize (ObjectMapper objectMapper) {

    }
}
