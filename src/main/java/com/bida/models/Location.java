package com.bida.models;

import java.io.Serializable;
import java.time.LocalDateTime;

public class Location implements Serializable {
    public Integer id;
    public String label;
    public Float latitude;
    public Float longitude;
    public LocalDateTime createdAt;
    public LocalDateTime deletedAt;
}
