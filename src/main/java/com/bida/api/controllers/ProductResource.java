package com.bida.api.controllers;

import com.bida.api.gateways.http.Endpoints;
import com.bida.models.vendor.ProductInventory;
import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import io.vertx.mutiny.pgclient.PgPool;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path(Endpoints.PRODUCTS)
public class ProductResource {

    @Inject
    PgPool client;

    @PostConstruct
    void config(){
        initdb();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Multi<ProductInventory> get(){
        return ProductInventory.findAll(client);
    }

    @GET
    @Path("/{id:[0-9]}")
    public Uni<ProductInventory> get(@PathParam("id") Long id){
        return ProductInventory.findByID(client, id);
    }

    private void initdb(){
        client.query("DROP TABLE IF EXISTS product_inventory").execute()
        .flatMap(m->client.query("CREATE TABLE product_inventory(id SERIAL PRIMARY KEY, vendor_id INT, product_id INT, quantity INT, sku SERIAL, price FLOAT, visibility BOOLEAN, status VARCHAR(15), created_at TIMESTAMP DEFAULT current_timestamp, updated_at TIMESTAMP DEFAULT null, deleted_at TIMESTAMP DEFAULT null)").execute())
                .flatMap(m -> client.query("INSERT INTO product_inventory(product_id, vendor_id, quantity, price, visibility, status) VALUES (1, 1, 4, 12000, true, 'In Stock'), (1, 2, 2, 18000, true, 'Running Low')").execute())
                .await()
                .indefinitely();
    }


}
