package com.bida.api.controllers;

import com.bida.api.gateways.http.Endpoints;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class Auth {

    @POST
    @Path(Endpoints.LOGIN)
    public void login(){
        //ToDo: Add logic
    }
}
