package com.bida.api.gateways.http;

/**
 * All reactive endpoints are held here
 * */
public class Endpoints {

    public static final String BASE_URL = "v1/user";
    public static final String SIGN_UP = "/register";
    public static final String LOGIN = "/login";
    public static final String SEND_CODE = "/send_code";
    public static final String VERIFY_NUMBER = "/verify";
    public static final String FORGOT_PASSWORD = "/forgot_password";
    public static final String RESET_PASSWORD = "/reset_password";
    public static final String EDIT_PROFILE = "/{id:[0-9]*}/profile/edit";
    public static final String NOTIFICATIONS = "/{id:[0-9]*}/notifications";
    public static final String SAVE_PRODUCT = "/{id:[0-9]*}/product/save";
    public static final String LOCATIONS = "/{id:[0-9]*}/locations";
    public static final String EDIT_LOCATIONS = "/{id:[0-9]*}/location/edit";
    public static final String SAVED_PRODUCTS = "/{id:[0-9]*}/product/saved";
    public static final String CART = "/{id:[0-9]*}/cart/{cardId:[0-9]*}";
    public static final String ADD_TO_CART = "/{id:[0-9]*}/cart/items";
    public static final String DELETE_CART_ITEM = "/{id:[0-9]*}/cart/items/{itemId:[0-9]*}";
    public static final String ORDERS = "/{id:[0-9]*}/orders";
    public static final String ORDER_HISTORY = "/{id:[0-9]*}/orders/{id:[0-9]}/history";
    public static final String ORDER_DETAILS = "/{id:[0-9]*}/order/{id:[0-9]*}/details";
    public static final String CANCEL_ORDER = "/{id:[0-9]*}/order/{id:[0-9]*}/cancel";
    public static final String MPESA_PAYMENT = "/{id:[0-9]*}/payment/mpesa";
    public static final String CARD_PAYMENT = "/{id:[0-9]*}/payment/card";
    public static final String PROMO_CODE_PAYMENT = "/{id:[0-9]*}/payment/promo";
    public static final String CASH_PAYMENT = "/{id:[0-9]*}/payment/cash";
    public static final String WALLET = "/{id:[0-9]*}/wallet";
    public static final String MPESA_WALLET = "/{id:[0-9]*}/wallet/mpesa";
    public static final String CARD_WALLET = "/{id:[0-9]*}/wallet/card";
    public static final String PROMO_WALLET = "/{id:[0-9]*}/wallet/promo";
    public static final String PRODUCTS = "/products";

    public static final String FETCH_CUSTOMERS = "/customers";
//    Vendor endpoints
    public static final String UPLOAD_PRODUCT_IMAGE = "/vendor/upload_product_image";
    public static final String ADD_PRODUCT = "vendor/product_details";

}
