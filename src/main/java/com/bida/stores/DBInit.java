package com.bida.stores;

import io.quarkus.runtime.StartupEvent;
import io.vertx.mutiny.pgclient.PgPool;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;

@ApplicationScoped
public class DBInit {

    private final PgPool client;
    private final boolean schemaCreate;

    public DBInit(PgPool client, @ConfigProperty(name = "myapp.schema.create", defaultValue = "true") boolean schemaCreate) {
        this.client = client;
        this.schemaCreate = schemaCreate;
    }

    void onStart(@Observes StartupEvent ev) {
        if (schemaCreate) {
            initdb();
        }
    }

    private void initdb() {
        client.query("DROP TABLE IF EXISTS users").execute()
                .flatMap(r -> client.query("CREATE TABLE users (id SERIAL PRIMARY KEY, name VARCHAR(20) NOT NULL)").execute())
                .flatMap(r -> client.query("INSERT INTO users (name) VALUES ('Erick')").execute())
                .flatMap(r -> client.query("INSERT INTO users (name) VALUES ('Stacy')").execute())
                .flatMap(r -> client.query("INSERT INTO users (name) VALUES ('Maxwell')").execute())
                .flatMap(r -> client.query("INSERT INTO users (name) VALUES ('Dorcas')").execute())
                .await().indefinitely();
    }
}